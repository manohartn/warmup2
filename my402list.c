#include<stdio.h>
#include<stdlib.h>

#include"my402list.h"
#include"cs402.h"

static My402ListElem* CreateAndInitializeMy402ListElem(void *obj)
{
	My402ListElem* elem = NULL;
	elem = (My402ListElem*) malloc(sizeof(My402ListElem));

	if (elem == NULL) 
	{
		/* Failure to Allocate memory */
		fprintf(stderr, "Error in allocating memory\n");
		return NULL;
	}
	elem->next = NULL;
	elem->prev = NULL;
	elem->obj  = obj;

	return elem;
}

/*Implementation for the functions defined in my402list.h*/

int My402ListLength(My402List *list)
{
	int length = list->num_members;
	return length;
}

int  My402ListEmpty(My402List *list)
{
	return (list->num_members <= 0);
}

int  My402ListAppend(My402List *list, void *obj)
{
	My402ListElem* lastElem = NULL;

	My402ListElem* elem = CreateAndInitializeMy402ListElem(obj);  	
	if (elem == NULL) {
		/* Something Failed */
		fprintf(stderr, "Error in allocating memory\n");
		return FALSE;
	}

	/* If list is empty just add obj to the list */
	if (My402ListEmpty(list)) {

		elem->next = &list->anchor;
		elem->prev = &list->anchor;

		list->anchor.next = elem;
		list->anchor.prev = elem;
	}
	else {
		lastElem = My402ListLast(list);

		lastElem->next = elem;

		elem->prev = lastElem;
		elem->next = &list->anchor;

		list->anchor.prev = elem;
	}
	/* Time to increment the list count */
	++list->num_members;
	return TRUE;
}

int  My402ListPrepend(My402List *list, void *obj)
{ 
	My402ListElem* firstElem = NULL;
	My402ListElem* elem = CreateAndInitializeMy402ListElem(obj);  	
	if (elem == NULL) {
		/* Something Failed */
		fprintf(stderr, "Error in allocating memory\n");
		return FALSE;
	}

	/* If list is empty just add obj to the list */
	if (My402ListEmpty(list)) {

		elem->next = &list->anchor;
		elem->prev = &list->anchor;

		list->anchor.next = elem;
		list->anchor.prev = elem;
	}
	else {
		firstElem = My402ListFirst(list);

		firstElem->prev = elem;

		elem->prev = &list->anchor;
		elem->next = firstElem;

		list->anchor.next = elem;
	}
	/* Time to increment the list count */
	++list->num_members;
	return TRUE; 
}

void My402ListUnlink(My402List *list, My402ListElem *elem)
{
	My402ListElem *prevElem = NULL;
	My402ListElem *nextElem = NULL;

	/* If no elements on the list nothing to do! */
	if (list->num_members == 0) 
	{
		return;
	}

	/*Make the pointers point accordingly */ 
	prevElem = elem->prev;
	nextElem = elem->next;

	prevElem->next = elem->next;
	nextElem->prev = elem->prev; 

	/* Time to decrement the list count by 1 and free up the resource used */ 
	--list->num_members;
	free((void*) elem);
}

void My402ListUnlinkAll(My402List *list)
{
	My402ListElem *elem = NULL;
	My402ListElem *temp = NULL;

	elem = My402ListFirst(list);
	/* Keep Unlinking elements one by one until the list is empty */

	while (elem != NULL) {
		temp = My402ListNext(list, elem);
		My402ListUnlink(list, elem);
		/* Point to the next element to be deleted */
		elem = temp;
	}
}

int  My402ListInsertAfter(My402List *list, void *obj, My402ListElem *elem)
{
	My402ListElem *newElement = NULL;

	/* If elem is NULL, it is same as Append() */
	if (elem == NULL) {
		return My402ListAppend(list, obj);
	}

	newElement = CreateAndInitializeMy402ListElem(obj);
	if (newElement == NULL) {
		/* something wrong */
		fprintf(stderr, "Error in allocating memory\n");
		return FALSE;
	}

	newElement->prev = elem;
	newElement->next = elem->next;

	elem->next->prev = newElement;
	elem->next = newElement;

	/* One node inserted succesfully, Time to increment the list counter */
	++list->num_members; 
	return TRUE;
}

int  My402ListInsertBefore(My402List *list, void *obj, My402ListElem *elem)
{
	My402ListElem *newElement = NULL;

	/* If elem is NULL, it is same as Prepend() */
	if (elem == NULL) {
		return My402ListPrepend(list, obj);
	}

	newElement = CreateAndInitializeMy402ListElem(obj);
	if (newElement == NULL) {
		fprintf(stderr, "Error in allocating memory\n");
		/* something wrong */
		return FALSE;
	}

	newElement->prev = elem->prev;
	newElement->next = elem;

	elem->prev->next = newElement;
	elem->prev = newElement;  

	/* One node inserted succesfully, Time to increment the list counter */
	++list->num_members; 
	return TRUE;
}

My402ListElem *My402ListFirst(My402List* list)
{
	My402ListElem *firstElem = NULL;

	if (My402ListEmpty(list)) {
		return NULL;
	}

	firstElem = list->anchor.next;
	return firstElem;
}

My402ListElem *My402ListLast(My402List *list)
{
	My402ListElem *lastElem = NULL;

	if (My402ListEmpty(list)) {
		return NULL;
	}

	lastElem = list->anchor.prev;
	return lastElem;
}

My402ListElem *My402ListNext(My402List *list, My402ListElem *elem)
{
	My402ListElem* nextElem = NULL;

	/* If the given element is last element there exists no next element */
	if (elem->next == &list->anchor) {
		return NULL;
	}
	nextElem = elem->next;
	return nextElem;
}

My402ListElem *My402ListPrev(My402List *list, My402ListElem *elem)
{
	My402ListElem* prevElem = NULL;

	/* If the given element is first element there exists no previous element */
	if (elem->prev == &list->anchor) {
		return NULL;
	}

	prevElem = elem->prev;
	return prevElem;
}


My402ListElem *My402ListFind(My402List *list, void *obj)
{
	My402ListElem* elem = NULL;

	for (elem = My402ListFirst(list) ;
			elem != NULL ;
			elem = My402ListNext(list, elem)) {
		if (elem->obj == obj) {
			return elem;
		} 
	}

	/* The object you're looking is not found in My402List! */
	return NULL;
}


int My402ListInit(My402List* list)
{
	/* Initialize the list with zero elements */
	list->num_members = 0;

	list->anchor.obj = NULL;
	list->anchor.prev = &(list->anchor);
	list->anchor.next = &(list->anchor);

	/* Initialization Successful */
	return TRUE; 
}
