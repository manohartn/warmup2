#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<getopt.h>
#include<string.h>
#include<ctype.h>
#include<math.h>

#include<sys/time.h>
#include<sys/types.h>
#include<sys/stat.h>

#include "my402list.h"
#include "globals.h"
#include "cs402.h"

#define DEBUG 0
#define TEN_POWER_SIX 1000000
#define THOUSAND 1000

static void ComputePerPacketTimes(double tokenRate, 
				  double packetRate,
				  double serviceRate)
{
	gTokenInterArrival  = 1/tokenRate * TEN_POWER_SIX;
	if (gTokenInterArrival > (10 * TEN_POWER_SIX)) { 
		gTokenInterArrival = 10 * TEN_POWER_SIX; 
	}

	gPacketInterArrival = 1/packetRate * TEN_POWER_SIX;
	if (gPacketInterArrival > (10 * TEN_POWER_SIX)) {
		gPacketInterArrival = 10 * TEN_POWER_SIX;
	}

	gServiceTimePerPacket = 1/serviceRate * TEN_POWER_SIX;
	if (gServiceTimePerPacket > (10 * TEN_POWER_SIX)) {
		gServiceTimePerPacket = 10 * TEN_POWER_SIX;
	}

	gTokensReqForThisPacket = gTokensPerPacket;
}

void cleanUp_packet_thread(void* arg)
{
	pthread_mutex_unlock(&syncMutex);
}

void cleanUp_token_thread(void* arg)
{
	pthread_mutex_unlock(&syncMutex);
}

static void InitializeDefaultEmulationParams()
{
	memset(gFileName, '\0', sizeof(gFileName));
	gTokensPerPacket = 3;
	gLambda = 0.5;
	gTotalPackets = 20;
	gBucketSize = 10;
	gServiceRate = 0.35;
	gTokenRate = 1.5;
}

static
void ValidateFile(char* pathname)
{
        struct stat fileStructure;
        if (stat(pathname, &fileStructure) != 0) {
                fprintf(stderr, "Malformed command : Either file doesn't exist or Error in opening file \"%s\" - Access Denied\n", pathname);
                exit(-1);
        }

        /* Check if filesize is 0 - No data present */
        if (fileStructure.st_size == 0) {
                fprintf(stderr, "The given file - %s is empty!. Please provide a valid file\n", pathname);
                exit(-1);

        }

        /* Check if the pathname is a directory */
        if (!(fileStructure.st_mode & S_IFREG))
        {
                fprintf(stderr, "Probably the specified file is not a regular file, it could be a directory or other\n");
                exit(-1);
        }
}

long int GetTimeNowIn_us() 
{
	long int totalMicroSeconds = 0;
	struct timeval tv;
	if (0 == gettimeofday(&tv, NULL)) {
		totalMicroSeconds = ((tv.tv_sec)*TEN_POWER_SIX) + tv.tv_usec;
		return totalMicroSeconds;
	}
	return 0;
}

void printTime(double time) 
{
	//double timeNowIn_ms = ((double)(GetTimeNowIn_us() - base_reference_time))/1000;
	fprintf(stdout, "%012.3lfms: ", time/1000);
}

static void Exit(char *msg)
{
	fprintf(stderr, "%s", msg);
	exit(-1);
}

static void UnlinkAndMoveTo_Q2(My402ListElem *elem, MyPacket *myPacket)
{
	//Decrement the tokens
	gTotalTokens -= ((MyPacket*)(elem->obj))->tokensRequired;

	char *ptrToStr;
	if (gTotalTokens > 1) {
		ptrToStr = tokens;	
	}
	else {
		ptrToStr = token;
	}
	double timeIn_Q1 = 0;
	myPacket->exitFrom_Q1 = (double)(GetTimeNowIn_us() - base_reference_time);
	timeIn_Q1 = (myPacket->exitFrom_Q1 - myPacket->arrivalIn_Q1);
	printTime(myPacket->exitFrom_Q1);
	fprintf(stdout, "p%d leaves Q1, time in Q1 = %.3lfms, token bucket now has %d %s\n",
			 myPacket->packetNumber, 
			 (timeIn_Q1)/1000,
			 gTotalTokens,
			 ptrToStr);

	int isList_Q2_empty = FALSE;

	My402ListUnlink(&List_Q1, elem);
	//Move Packet to Q2;

	isList_Q2_empty = My402ListEmpty(&List_Q2);
	My402ListAppend(&List_Q2, (void*)myPacket);
	++gPacketsIn_Q2;
	myPacket->arrivalIn_Q2 = (double)(GetTimeNowIn_us() - base_reference_time);
	
	printTime(myPacket->arrivalIn_Q2);
	fprintf(stdout, "p%d enters Q2\n", myPacket->packetNumber);
	//Check if list Q2 is empty
	if (isList_Q2_empty) {
		pthread_cond_signal(&Q2_NotEmpty);
		//signal or broadcast
	}
}

int ValidateForNumber(char *string)
{
	int i = 0;
	for (i=0; i < strlen(string); i++) {
		if (isdigit(string[i]) == 0) {
			return FALSE;
		}
	}
	return TRUE;
}

static void AddToList_Q1(MyPacket *myPacket)
{
	if (My402ListAppend(&List_Q1, (void*)myPacket)) 
	{
		myPacket->arrivalIn_Q1 = (double) (GetTimeNowIn_us() - base_reference_time);
		printTime(myPacket->arrivalIn_Q1);
		fprintf(stdout, "p%d enters Q1\n", gPacketsProcessed);
		++gPacketsIn_Q1;
		myPacket->packetNumber = gPacketsProcessed;
		myPacket->tokensRequired = gTokensReqForThisPacket;
		myPacket->serviceTime = gServiceTimePerPacket;
	}
	else
	{
		fprintf(stdout, "Failure in adding the packet to list Q1\n");
	}
}

static void DropPacket()
{
	fprintf(stdout, ", dropped\n");
	gPacketsDropped++;
}

static void CheckFirstAndLastChar(int lineNo, char *buf)
{
	//TODO: Each line ends with a new line char
	if (0 == isdigit(buf[0])) {
		fprintf(stderr, "In the line %d of file, First char is not valid\n", lineNo+1);
		exit(-1);
	}
	if (0 == isdigit(buf[strlen(buf) - 1])) {
		fprintf(stderr, "In the line %d of file, Last char is not valid\n", lineNo+1);
		exit(-1);
	}
}

static void ReadInput(int lineNo)
{

	char buf[1026];
	char* newLinePtr = NULL;
	char* token = NULL;
	memset(buf, '\0', sizeof(buf));
	if (NULL != fgets(buf, sizeof(buf), gInputFile))
	{
		newLinePtr = strchr(buf, '\n');
		if (NULL != newLinePtr) 
		{
			*newLinePtr = '\0';
		}
		else 
		{
			fprintf(stderr, "Line %d in the file is not terminated with a newline char\n", lineNo+1);
			exit(-1);
		}
		CheckFirstAndLastChar(lineNo, buf);
		token = strtok(buf, "\t ");
		/* lambda, Packet Inter Arrival Time */
		if (NULL == token) 
		{
			Exit("ERROR: Input file not in the right format\n");
		} 
		if(ValidateForNumber(token)) 
		{
			int val = atoi(token);
			if (val) 
			{
				pthread_mutex_lock(&syncMutex);
				gPacketInterArrival = 1000*val; // in micro seconds
#if DEBUG
				fprintf(stdout, "From FILE: Packet Inter-arrival Time = %d \n",
					 	(int)gPacketInterArrival);
#endif
				gLambda = (1/gPacketInterArrival)*TEN_POWER_SIX; 
				pthread_mutex_unlock(&syncMutex);
			}
		}
		else 
		{
			fprintf(stderr, "Error : Input not in right format - (lambda) at line No %d in the file %s\n", lineNo, gFileName);
			exit(-1);
		}

		/* P, Tokens Per Packet */
		token = strtok(NULL, "\t ");
		if (NULL == token) 
		{
			Exit("ERROR: Input file not in the right format\n");
		} 
		if(ValidateForNumber(token)) 
		{
			int val = atoi(token);
			if (val) 
			{
				pthread_mutex_lock(&syncMutex);
				gTokensReqForThisPacket = val; // in micro seconds
				gTokensPerPacket = val;
#if DEBUG
				fprintf(stdout, "From FILE: Tokens Req for this packet = %d \n",
					 	(int)gTokensReqForThisPacket);
#endif
				pthread_mutex_unlock(&syncMutex);
			}
		}
		else 
		{
			fprintf(stderr, "Error : Input not in right format - (P-tokens per packet) at line No %d in the file %s\n", lineNo, gFileName);
			exit(-1);
		}


		/*mu, ServiceTime */
		token = strtok(NULL, "\t ");
		if (NULL == token) 
		{
			Exit("ERROR: Input file not in the right format\n");
		} 
		if(ValidateForNumber(token)) 
		{
			int val = atoi(token);
			if (val) 
			{
				pthread_mutex_lock(&syncMutex);
				gServiceTimePerPacket = 1000*val; // in micro seconds
				gServiceRate = (double)(1/gServiceTimePerPacket)*TEN_POWER_SIX;
#if DEBUG
				fprintf(stdout, "From FILE: Service Time = %d \n",
					 	(int)gServiceTimePerPacket);
#endif
				pthread_mutex_unlock(&syncMutex);
			}
		}
		else 
		{
			fprintf(stderr, "Error : Input not in right format - (mu - service time) at line No %d in the file %s\n", lineNo, gFileName);
			exit(-1);
		}

		token = strtok(NULL, "/t ");
		if (NULL != token) 
		{
			Exit("ERROR: Input file not in the right format - More spaces or tabs than required\n");
		} 
	}
}

static void ComputeStatistics()
{
	gCompletedPackets = gPacketsIn_Q1 + gPacketsDropped; 

	if (0 != gPacketsProcessed) {
		gAvgInterArrivalTime = (gTotalInterArrivalTime/gPacketsProcessed);
	}
	else {
		gAvgInterArrivalTime = -1;
	}
	
	if (gPacketsServed != 0) {
		gAvgPacketServiceTime = (gTotalPacketServiceTime/gPacketsServed);
	}
	else {
		gAvgPacketServiceTime = -1;	
	}

	if (0 != gEmulationTime) {
		gAvgPacketsIn_Q1 = (gTotalTimeOfAllPacketsIn_Q1/gEmulationTime);
		gAvgPacketsIn_Q2 = (gTotalTimeOfAllPacketsIn_Q2/gEmulationTime);
		gAvgPacketsAt_S  = (gTotalPacketServiceTime/gEmulationTime);
	}
	else {
		gAvgPacketsIn_Q1 = -1;
		gAvgPacketsIn_Q2 = -1;
		gAvgPacketsAt_S = -1;
	}

	if (0 != gPacketsServed) {
		gAvgTimePacketSpentInSys = (gTotalTimeInSystem/gPacketsServed);
		gVariance_TotalTimeInSys = (gTotalTimeSpentInSystem_SQUARE/gPacketsServed) - (gAvgTimePacketSpentInSys*gAvgTimePacketSpentInSys);
		gStandardDeviation_TimeSpentInSys = sqrt(gVariance_TotalTimeInSys);
	}
	else {
		gAvgTimePacketSpentInSys = -1;
		gStandardDeviation_TimeSpentInSys = -1;
	}

	if (0 != gTotalTokensProcessed) {
		gTokenDropProbability  = ((double)gTokensDropped/gTotalTokensProcessed);
	}
	else {
		gTokenDropProbability = -1;
	}
	if (0 != gCompletedPackets) {
		gPacketDropProbability = ((double)gPacketsDropped/gCompletedPackets);	
	}
	else {
		gPacketDropProbability = -1;
	}
}

static void DisplayStatistics()
{
	char NA[] = "NA";
	
	fprintf(stdout, "\n");
	fprintf(stdout, "Statistics:\n");
	if (-1 != gAvgInterArrivalTime) {
        	fprintf(stdout, "	average packet inter-arrival time = %lf\n", gAvgInterArrivalTime/TEN_POWER_SIX);
	}
	else {
        	fprintf(stdout, "	average packet inter-arrival time = %s\n", NA);
	}
	
	if (-1 != gAvgPacketServiceTime) {
        	fprintf(stdout, "	average packet service time = %lf\n", gAvgPacketServiceTime/TEN_POWER_SIX);
	}
	else {
        	fprintf(stdout, "	average packet service time = %s\n", NA);
	}
	fprintf(stdout, "\n");
   
	if (0 != gEmulationTime) {
        	fprintf(stdout, "	average number of packets in Q1 = %lf\n", gAvgPacketsIn_Q1);
        	fprintf(stdout, "	average number of packets in Q2 = %lf\n", gAvgPacketsIn_Q2);
        	fprintf(stdout, "	average number of packets at S  = %lf\n", gAvgPacketsAt_S);
	}
	else {
        	fprintf(stdout, "	average number of packets in Q1 = %s\n", NA);
        	fprintf(stdout, "	average number of packets in Q2 = %s\n", NA);
        	fprintf(stdout, "	average number of packets at S  = %s\n", NA);
	} 
	fprintf(stdout, "\n");

	if (-1 != gAvgTimePacketSpentInSys) {
		fprintf(stdout, "	average time a packet spent in system = %lf\n", gAvgTimePacketSpentInSys/TEN_POWER_SIX);
	}
	else {
		fprintf(stdout, "	average time a packet spent in system = %s\n", NA);
	}

	if (-1 != gStandardDeviation_TimeSpentInSys) {
		fprintf(stdout, "	standard deviation for time spent in system = %lf\n", gStandardDeviation_TimeSpentInSys/TEN_POWER_SIX);
	}
	else {
		fprintf(stdout, "	standard deviation for time spent in system = %s\n", NA);
	}
   
	if (-1 != gTokenDropProbability) {
        	fprintf(stdout, "	token drop probability = %lf\n", gTokenDropProbability);
	}
	else {
        	fprintf(stdout, "	token drop probability = %s\n", NA);
	}

	if (-1 != gPacketDropProbability) {
        	fprintf(stdout, "	packet drop probability = %lf\n", gPacketDropProbability);
	}
	else {
        	fprintf(stdout, "	packet drop probability = %s\n", NA);
	}
}

static void* Start_Packet_Thread() 
{
	long int sleepTime = 0;
	long int packetArrival = 0;
	long int prevPacketArrival = base_reference_time;
	long int packetInterArrival = 0;
	MyPacket *myPacket = NULL;
	static int linesRead = 1;

	int i = 0;
	pthread_cleanup_push(cleanUp_packet_thread, (void*)&i);

	while (TRUE && !gShutdown) {
		
		if (gFileSpecified && linesRead <= gTotalPackets) 
		{
			ReadInput(linesRead);
			++linesRead;
				
		}
			
		pthread_mutex_lock(&syncMutex);
		if (gPacketsProcessed >= gTotalPackets) {
			// All the packets have been processed, exit the thread
			pthread_mutex_unlock(&syncMutex);
			break;	
		}				
		pthread_mutex_unlock(&syncMutex);
			
		sleepTime = gPacketInterArrival - (GetTimeNowIn_us() - prevPacketArrival);
		if (sleepTime > 0) {
			usleep((useconds_t)sleepTime);	
		}
		pthread_testcancel();
		pthread_mutex_lock(&syncMutex);
			++gPacketsProcessed;
			packetArrival = GetTimeNowIn_us();
			packetInterArrival = (packetArrival - prevPacketArrival);
			gTotalInterArrivalTime += packetInterArrival;
			printTime(packetArrival - base_reference_time);
			char *strPtr = token;
			if (gTokensReqForThisPacket > 1) {
				strPtr = tokens;  
			} 
			fprintf(stdout, "p%d arrives, needs %d %s, inter-arrival time = %.3lfms", 
					gPacketsProcessed, gTokensReqForThisPacket, strPtr, (double)packetInterArrival/1000);	
			prevPacketArrival = packetArrival;
			pthread_testcancel();
			if (gTokensReqForThisPacket > gBucketSize) {
				DropPacket();
				pthread_testcancel();
				pthread_mutex_unlock(&syncMutex);
				continue;
			}
			else {
				fprintf(stdout, "\n");
				pthread_testcancel();
				myPacket = (MyPacket*) malloc(sizeof(MyPacket));
				myPacket->interArrivalTime = (double)packetInterArrival;
				AddToList_Q1(myPacket);
				pthread_testcancel();

				// Dequeue first packet and check it it can be moved to List Q2				
					
				My402ListElem *elem = My402ListFirst(&List_Q1);
				if (gTotalTokens >= ((MyPacket*)(elem->obj))->tokensRequired) 
				{
					UnlinkAndMoveTo_Q2(elem, (MyPacket*)(elem->obj));
					pthread_testcancel();
						
				}
			}
		pthread_mutex_unlock(&syncMutex);
	}
	pthread_cleanup_pop(0);
	pthread_mutex_unlock(&syncMutex);
	pthread_mutex_lock(&syncMutex);
	pthread_cond_signal(&Q2_NotEmpty);
	gPacketThreadExit = TRUE;
	pthread_mutex_unlock(&syncMutex);
	pthread_exit((void*)0);
}

static void RemoveAllPacketsIn_List(My402List* list, char *listName)
{
	My402ListElem* elem = NULL;

	for (elem = My402ListFirst(list) ;
			elem != NULL ;
			elem = My402ListNext(list, elem)) {
		/*Unlink All Bank Transaction records */
		int packetNum = ((MyPacket*)(elem->obj))->packetNumber;
		printTime(GetTimeNowIn_us() - base_reference_time);
		fprintf(stdout, "p%d removed from %s\n", packetNum, listName);
		free(elem->obj);
	}
	My402ListUnlinkAll(list);
}

static void CleanUp() 
{
	if (gInputFile) {
		fclose(gInputFile);
		gInputFile = NULL;
	}
	while (!My402ListEmpty(&List_Q1)) 
	{
		RemoveAllPacketsIn_List(&List_Q1, "Q1");
			
	}
	while (!My402ListEmpty(&List_Q2))
	{
		RemoveAllPacketsIn_List(&List_Q2, "Q2");
		
	}
}
static void* Token_Thread() 
{
	long int tokenArrivalTime = base_reference_time;
	long int sleepTime = 0;
	int i = 0;
	pthread_cleanup_push(cleanUp_token_thread, (void*)&i);
	while (TRUE && !gShutdown) 
	{
		pthread_mutex_lock(&syncMutex);
		if (My402ListEmpty(&List_Q1) &&
		    gPacketsProcessed == gTotalPackets) {
			// Time to exit token depositing thread
			pthread_mutex_unlock(&syncMutex);
			break;
		}
		pthread_mutex_unlock(&syncMutex);
		
		sleepTime = gTokenInterArrival - (GetTimeNowIn_us() - tokenArrivalTime);
			
		if (sleepTime > 0) 
		{
			usleep((useconds_t)sleepTime);
		}
		pthread_testcancel();
		tokenArrivalTime = GetTimeNowIn_us();
		pthread_mutex_lock(&syncMutex);
		if (My402ListEmpty(&List_Q1) && 
		    gPacketsProcessed == gTotalPackets) {
			// Time to exit token depositing thread
			pthread_mutex_unlock(&syncMutex);
			break;
		}
		++gTotalTokensProcessed;
		pthread_testcancel();
		if (gTotalTokens >= gBucketSize) 
		{
			// Overflow
			++gTokensDropped;
			printTime(GetTimeNowIn_us() - base_reference_time);
			fprintf(stdout, "token t%d arrives, dropped\n", gTotalTokensProcessed);
			pthread_testcancel();
				
			pthread_mutex_unlock(&syncMutex);
			continue;	
		}
		
		++gTotalTokens;
		char *ptrToStr = NULL;
		if (gTotalTokens > 1) {
			ptrToStr = tokens;
		}
		else {
			ptrToStr = token;
		}
		pthread_testcancel();
		printTime(tokenArrivalTime - base_reference_time);
		fprintf(stdout, "token t%d arrives, token bucket now has %d %s\n", gTotalTokensProcessed, gTotalTokens, ptrToStr);
		pthread_testcancel();
			
		if (!My402ListEmpty(&List_Q1)) {
			My402ListElem *elem = My402ListFirst(&List_Q1);
			int tokensReq = (int)(((MyPacket*)(elem->obj))->tokensRequired);
			if (gTotalTokens >= tokensReq) {
				// Move the packet to List Q2 and decrement tokens
				UnlinkAndMoveTo_Q2(elem, ((MyPacket*)(elem->obj)));
				pthread_testcancel();
			}			
		}
		pthread_testcancel();
		pthread_mutex_unlock(&syncMutex);
	}
	pthread_cleanup_pop(0);
	pthread_mutex_unlock(&syncMutex);
	pthread_mutex_lock(&syncMutex);
		pthread_cond_signal(&Q2_NotEmpty);
		gTokenThreadExit = TRUE;
	pthread_mutex_unlock(&syncMutex);	
	pthread_exit((void*)0);
}

static void UnlinkFrom_Q2(My402ListElem *elem)
{
	My402ListUnlink(&List_Q2, elem);
	--gPacketsIn_Q2;	
}

static void ServePacket(MyPacket *myPacket)
{
	double serviceTime = 0;
	double timeInSystem = 0;

	myPacket->serviceEntry = (GetTimeNowIn_us() - base_reference_time);
	printTime(myPacket->serviceEntry);
	fprintf(stdout, "p%d begins service at S, requesting %.3lfms of service\n",
			(myPacket->packetNumber),
			(myPacket->serviceTime)/1000);
	pthread_mutex_unlock(&syncMutex);
	usleep((useconds_t)myPacket->serviceTime);
	pthread_mutex_lock(&syncMutex);
	myPacket->serviceExit = (GetTimeNowIn_us() - base_reference_time);

	serviceTime = (myPacket->serviceExit - myPacket->serviceEntry);
	timeInSystem = (myPacket->serviceExit - myPacket->arrivalIn_Q1);
	
	printTime(myPacket->serviceExit);
	fprintf(stdout, "p%d departs from S, service time = %.3lfms, time in system = %.3lfms\n",
	 		myPacket->packetNumber,
			(serviceTime)/1000,
			(timeInSystem)/1000);
	++gPacketsServed;
	
	gTotalPacketServiceTime += serviceTime;
	gTotalTimeInSystem += timeInSystem;
	gTotalTimeSpentInSystem_SQUARE += (timeInSystem * timeInSystem);
	gTotalTimeOfAllPacketsIn_Q1 += (myPacket->exitFrom_Q1 - myPacket->arrivalIn_Q1);
	gTotalTimeOfAllPacketsIn_Q2 += (myPacket->exitFrom_Q2 - myPacket->arrivalIn_Q2);

	// Now free the memory of the packet
	free(myPacket);
}

static void EndEmulation()
{
	CleanUp();
	gEmulationTime = GetTimeNowIn_us() - base_reference_time;
	printTime(gEmulationTime);
	fprintf(stdout, "emulation ends\n");
}

static void* Start_Server_Thread() 
{
	double timeIn_Q2 = 0;
	while (TRUE && !gShutdown) 
	{
		pthread_mutex_lock(&syncMutex);

		if  (gPacketsServed == (gTotalPackets - gPacketsDropped))
		{
			//All the packets are served. Exit!
			pthread_mutex_unlock(&syncMutex);
			break;
		}	

		while (!gPacketsIn_Q2 && 
		       !gShutdown && 
		       !gPacketThreadExit &&
		       !gTokenThreadExit) 
		{
			// condition wait
			pthread_cond_wait(&Q2_NotEmpty, &syncMutex);
			usleep(1000);
		}

		if (gShutdown) {
			pthread_mutex_unlock(&syncMutex);
			break;
		}
		
		if (!My402ListEmpty(&List_Q2) && !gShutdown) {
			My402ListElem *elem = My402ListFirst(&List_Q2);
			MyPacket *myPacket = ((MyPacket*)(elem->obj));
			
			// packet in Q2 exits
			myPacket->exitFrom_Q2 = (GetTimeNowIn_us() - base_reference_time);
			timeIn_Q2 = (myPacket->exitFrom_Q2 - myPacket->arrivalIn_Q2);
			printTime(myPacket->exitFrom_Q2);
			fprintf(stdout, "p%d leaves Q2, time in Q2 = %.3lfms\n", 
					myPacket->packetNumber,
					(timeIn_Q2)/1000);
			UnlinkFrom_Q2(elem);
			if (!gShutdown)
			{		
				ServePacket(myPacket);
			}
			else 
			{
				printTime(GetTimeNowIn_us() - base_reference_time);
				fprintf(stdout,"p%d removed from Q2\n", myPacket->packetNumber);
				free(myPacket);
			}
		}
		else if (!My402ListEmpty(&List_Q1) && !gShutdown) {
			/* Check if it needs to wait more */
			pthread_mutex_unlock(&syncMutex);
			continue;
		}
		else {
			pthread_mutex_unlock(&syncMutex);
			break;	

		}
		if (gShutdown) { 
			pthread_mutex_unlock(&syncMutex);
			break;
		}
		pthread_mutex_unlock(&syncMutex);
	}
	pthread_mutex_unlock(&syncMutex);
	pthread_mutex_lock(&syncMutex);
	EndEmulation();
	gServerThreadExit = TRUE;	
	pthread_mutex_unlock(&syncMutex);
	pthread_exit((void*)0);
}

static void SIGINT_Handler(int signal)
{
	pthread_cancel(t1);
	pthread_cancel(t2);

	pthread_mutex_lock(&syncMutex);
	gShutdown = TRUE;
	gPacketThreadExit = TRUE;
	gTokenThreadExit = TRUE;

	pthread_cond_signal(&Q2_NotEmpty);
	pthread_mutex_unlock(&syncMutex);
}

static void *Signal_Handler_Thread()
{
	act.sa_handler = SIGINT_Handler;
	sigaction(SIGINT, &act, NULL);
	pthread_sigmask(SIG_UNBLOCK, &newSigSet, NULL);
	while (!(gPacketThreadExit && gTokenThreadExit && gServerThreadExit)) 
	{
		sleep(2);
	}
	pthread_exit((void*)0);
}

static void CreateChildThreads( pthread_t *t1,
				pthread_t *t2,
				pthread_t *t3,
				pthread_t *signalThread) 
{
	if (0 != pthread_create(t1, 0, (void*)&Start_Packet_Thread, NULL)) {
		fprintf(stdout, "Error in creating thread 1\n");
	}
	
	/* Thread t2 */
	if (0 != pthread_create(t2, 0, (void*)&Token_Thread, NULL)) {
		fprintf(stdout, "Error in creating thread 2\n");
	}

	/* Thread t3 */
	if (0 != pthread_create(t3, 0, (void*)&Start_Server_Thread, NULL)) {
		fprintf(stdout, "Error in creating thread 3\n");
	}

	/* Signal thread */
	if (0 != pthread_create(signalThread, 0, (void*)&Signal_Handler_Thread, NULL)) {
		fprintf(stdout, "Error in creating thread 3\n");
	}
}

static void WaitForChildThreads(pthread_t *t1, 
				pthread_t *t2,
				pthread_t *t3,
				pthread_t *signalThread)
{
	if (0 != pthread_join(*t1, NULL))	
	{
		fprintf(stdout, "Failure while main thread trying to join to child thread 1");
	}

	if (0 != pthread_join(*t2, NULL))	
	{
		fprintf(stdout, "Failure while main thread trying to join to child thread 1");
	}

	if (0 != pthread_join(*t3, NULL))	
	{
		fprintf(stdout, "Failure while main thread trying to join to child thread 3");
	}
	if (0 != pthread_join(*signalThread, NULL))	
	{
		fprintf(stdout, "Failure while main thread trying to join to signal Thread");
	}
}


static int Valid_val(double val)
{
	if (val == 0)
		return FALSE;
	return TRUE;
}

static void Usage()
{
	char usage[] = "warmup2 [-lambda lambda] [-mu mu] [-r r] [-B B] [-P P] [-n num] [-t tsfile]";
	fprintf(stdout, "Usage:\n%s\n", usage);
}

static void ExitAndPrintUsage(char *msg)
{

	if (NULL != msg)	
	{
		fprintf(stderr, "%s", msg);	
	}
	Usage();
	exit(-1);
}

static void ParseCommandLineArgs(char **argv, char c) 
{

	switch(c) 
	{
		case 'l':
			if (NULL == argv[optind]) 	
			{
				ExitAndPrintUsage("lambda value cannot be empty\n");
			}
			gLambda = atof(argv[optind]);
			if(!Valid_val(gLambda)) 
			{
				ExitAndPrintUsage("ERROR: Malformed command, Lambda INVALID\n");
			}
			break;
		case 'B':
			
			if (!ValidateForNumber(optarg)) {
				Exit("B - Bucket Size is not an integer (not a valid format)\n");
			}
			gBucketSize = atof(optarg);
			if (!Valid_val(gBucketSize))
			{
				ExitAndPrintUsage("ERROR: Malformed command, Bucket Size INVALID \n");
			}
			break;
		case 'r':
			gTokenRate = atof(optarg);
			if (!Valid_val(gTokenRate))
			{
				ExitAndPrintUsage("ERROR: Malformed command, Token Rate INVALID \n");
			}
			break;
		case 'P':
			if (!ValidateForNumber(optarg)) {
				Exit("P - tokens required per packet is not an integer (not a valid format)\n");
			}
			gTokensPerPacket = atof(optarg);
			if (!Valid_val(gTokensPerPacket))
			{
				ExitAndPrintUsage("ERROR: Malformed command, Tokens Per Packet value INVALID \n");
			}
			break;
		case 'n':
			if (!ValidateForNumber(optarg)) {
				Exit("N - Total No. of Packets is not an integer (not a valid format)\n");
			}
			gTotalPackets = atof(optarg);
			if (!Valid_val(gTotalPackets))
			{
				ExitAndPrintUsage("ERROR: Malformed command, Total Packets INVALID \n");
			}
			break;
		case 'm':
			if ((argv[optind]) == NULL) 
			{
				ExitAndPrintUsage("Service Rate cannot be empty\n");
			}
			gServiceRate = atof(argv[optind]);
			fprintf(stdout, "gServiceRate = %lf\n", gServiceRate);
			if (!Valid_val(gServiceRate))
			{
				ExitAndPrintUsage("ERROR: Malformed command, Service Rate (mu) INVALID \n");
			}
			break;
		case 't':
			gFileSpecified = TRUE;
			strcpy(gFileName, argv[optind]);
			ValidateFile(gFileName);
			break;
		default:
			Usage();
			exit(-1);
	}
}

void PrintEmulationParameters()
{
	fprintf(stdout, "Emulation Parameters\n");
 	fprintf(stdout, "	number to arrive = %d\n", (int)gTotalPackets);
	if (!gFileSpecified) 
	{ 
        	fprintf(stdout, "	lambda = %lf\n", gLambda);
        	fprintf(stdout, "	mu = %lf\n", gServiceRate);
	}
        fprintf(stdout, "	r = %lf\n", gTokenRate);
        fprintf(stdout, "	B = %d\n",(int)gBucketSize);
	if (!gFileSpecified)
	{
        	fprintf(stdout, "	P = %d\n", (int)gTokensPerPacket);
	}
	if  (gFileSpecified)
	{
		fprintf(stdout, "	tsfile = %s\n", gFileName);
	}
}

static void 
	ValidateEmulationParams(double gLambda, 
				double gServiceRate, 
				double gTokenRate, 
				int gBucketSize, 
				int gTokensPerPacket, 
				int gTotalPackets)
{
	/* B, P and num must be positive integers */
	if (((gBucketSize - (int)gBucketSize) != 0) || 
             (gBucketSize <= 0))	
		Exit("Bucket Size (B) is not a positive integer\n");
	if (((gTokensPerPacket - (int)gTokensPerPacket) != 0) ||
	     (gTokensPerPacket <= 0))
		Exit("Tokens Per Packet (P) is not a positive integer\n");
	if (((gTotalPackets - (int)gTotalPackets) != 0) ||
	     (gTotalPackets <= 0))
		Exit("Total Packets (num) is not a positive integer\n");

	if (gBucketSize > 2147483647) 	{
		Exit("Bucket Size exceeds value 2147483647\n");
	}
	if (gTokensPerPacket > 2147483647) {
		Exit("Tokens Per Packet exceeds max value 2147483647\n");
	}
	if (gTotalPackets > 2147483647 ) {
		Exit("Total Packets exceeds max value 2147483647\n");
	}
	if (gLambda <= 0) {
		Exit("Lambda is not a positive real number\n");
	}
	if (gServiceRate <= 0) {
		Exit("Service rate is not a positive real number\n");
	}
	if (gTokenRate <=0) {
		Exit("Token Rate is not a positive real number\n");
	}
}

static void BlockSignal_SIGINT()
{
	sigemptyset(&newSigSet);
	sigaddset(&newSigSet, SIGINT);
	pthread_sigmask(SIG_BLOCK, &newSigSet, NULL);
}

static void CheckForInvalidParameters(int argc, char **argv)
{
	if (argc > 1) {
		if (!(argv[1][0] == '-'))
		{
			ExitAndPrintUsage("Invalid Arguments\n");
		}
	}		
}

int main(int argc, char* argv[])
{
	int optIndex;

	My402ListInit(&List_Q1);
	My402ListInit(&List_Q2);

	InitializeDefaultEmulationParams();

	CheckForInvalidParameters(argc, argv);
	char c;
	while ((c = getopt_long(argc, argv, "l:r:m:B:P:n:t", long_options, &optIndex)) != -1) 
	{
		ParseCommandLineArgs(argv, c);
	}
	ValidateEmulationParams(gLambda, gServiceRate, gTokenRate, gBucketSize, 
				gTokensPerPacket, gTotalPackets);
	ComputePerPacketTimes(gTokenRate, gLambda, gServiceRate); 

	if (gFileSpecified) 
	{
		gInputFile = fopen(gFileName, "r");
		if (gInputFile == NULL) {
			fprintf(stderr, "Error in opening file %s\n", gFileName);
			exit(-1);
		}
		char buf[1026];
		memset(buf, '\0', sizeof(buf));
		if (NULL != fgets(buf, sizeof(buf), gInputFile))
		{
			char *newLinePtr = strchr(buf, '\n');
			if (NULL != newLinePtr) 
			{
				*newLinePtr = '\0';
			}
			else {
				Exit("ERROR: The first line in the file doesn't contain a newline - Malformed input file\n");
			}
			if (ValidateForNumber(buf)) 
			{
				gTotalPackets = atoi(buf);
#if DEBUG
				fprintf(stdout, "From FILE: TotalPackets = %d \n",(int)gTotalPackets);
#endif
			}
			else 
			{
				Exit("ERROR: The first line in the file does not contain a valid number\n");
			}
			
		}	
		else 
		{
			Exit("ERROR: The first line - It may not contain valid <num> packets\n");
		}
	}
	PrintEmulationParameters();
	BlockSignal_SIGINT();

	base_reference_time = GetTimeNowIn_us();
	/* create the threads */
	CreateChildThreads(&t1, &t2, &t3, &signalThread);
	fprintf(stdout, "00000000.000ms: emulation begins\n");

	WaitForChildThreads(&t1, &t2, &t3, &signalThread);
	CleanUp();
	ComputeStatistics();
	DisplayStatistics();	
	return 0;
}
