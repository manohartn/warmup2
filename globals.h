#ifndef GLOBALS_H
#define GLOBALS_H

#include<pthread.h>
#include<signal.h>

/* Global variables */

pthread_t t1;
pthread_t t2;	
pthread_t t3;
pthread_t signalThread;

long int base_reference_time = 0;

double gTokensPerPacket = 0; //Tokens per packet
double gLambda = 0; //Packet interarrival
double gTotalPackets = 0; // total packets
double gBucketSize = 0; //Bucket Size
double gServiceRate = 0;
double gTokenRate = 0;

double gTokenInterArrival = 0;
double gPacketInterArrival = 0;
double gServiceTimePerPacket = 0;

int gPacketsIn_Q1 = 0;
int gPacketsIn_Q2 = 0;
int gPacketsServed = 0;

int gPacketsDropped = 0;
int gTokensDropped = 0;

int gTotalTokens = 0;;


double gTotalInterArrivalTime = 0;
double gTotalPacketServiceTime = 0;
double gTotalTimeInSystem = 0;

double gTotalTimeOfAllPacketsIn_Q1 = 0;
double gTotalTimeOfAllPacketsIn_Q2 = 0;

double gEmulationTime = 0;

/* Number of packets processed at any given time 
* increment in when either Q1 thread or the token thread
* moves packet to list Q2 */
 
int gPacketsProcessed;
int gTotalTokensProcessed;

int gTokensReqForThisPacket;

char token[] = "token";
char tokens[] = "tokens";

struct option long_options[] = {
	{"lambda", required_argument, 0, 'l'},
	{"mu"    , required_argument, 0, 'm'},
	{NULL, 0, NULL, 0}
};

typedef struct myPacketStruct
{
	int packetNumber;
	int tokensRequired;
	double serviceTime;
	double arrivalIn_Q1;
	double exitFrom_Q1;
	double interArrivalTime;
	double arrivalIn_Q2;
	double exitFrom_Q2;
	double serviceEntry;
	double serviceExit;
}MyPacket;

My402List List_Q1;
My402List List_Q2;

pthread_mutex_t syncMutex = PTHREAD_MUTEX_INITIALIZER; 
pthread_cond_t Q2_NotEmpty = PTHREAD_COND_INITIALIZER;

FILE* gInputFile = NULL;
char gFileName[1024] = "";  

int gFileSpecified = FALSE;

sigset_t newSigSet;
struct sigaction act;

int gShutdown = FALSE;

int gPacketThreadExit = FALSE;
int gTokenThreadExit = FALSE;
int gServerThreadExit = FALSE; 

double gCompletedPackets = 0;
double gAvgInterArrivalTime = 0;
double gAvgPacketServiceTime = 0;

double gAvgPacketsIn_Q1 = 0;
double gAvgPacketsIn_Q2 = 0;
double gAvgPacketsAt_S = 0;

double gAvgTimePacketSpentInSys;
double gTokenDropProbability = 0;
double gPacketDropProbability = 0;
double gTotalTimeSpentInSystem_SQUARE = 0;
double gVariance_TotalTimeInSys = 0;
double gStandardDeviation_TimeSpentInSys = 0;
#endif
